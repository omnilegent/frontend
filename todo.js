/*
 *      1. Convert containers into components and keep things in state
 *      2. Allow only unique emails
 *      3. Remove errors reducer and keep errors in main reducer, use comments to organize things
 *      4. Create font mixins
 * DONE 5. Fix SignupForm and LoginForm validation
 *      6. Fix ModalForm input and Modal box-shadow and border
 */
