const errorsReducer = (state = [], action) => {
	switch (action.type) {
		// Form-related errors like invalid email, no confirm password input and passwords don't match, or no matching use found
		case "ERROR_ADD":
			console.log("ERROR_ADD in errors reducer");
			return [...state, action.errorMsg];
		// remove an existing error from the errors array
		case "ERROR_REMOVE":
			console.log("ERROR_REMOVE in errors reducer");
			return state
				.slice(0, state.indexOf(action.errorMsg))
				.concat(state.slice(state.indexOf(action.errorMsg) + 1));
		// empty errors array
		case "ERROR_RESET":
			console.log("ERROR_RESET in errors reducer");
			return [];
		default:
			// console.log("default in errors reducer");
			return state;
	}
};

export default errorsReducer;
