import { fork, all } from "redux-saga/effects";
import createUserWatcher from "./createUser";
import userLoginWatcher from "./loginUser";
import fetchConversationsWatcher from "./conversations/fetchConversations";
import createConversationsWatcher from "./conversations/createConversation";
import viewConversationWatcher from "./conversations/viewConversation";
import editConversationWatcher from "./conversations/editConversation";
import deleteConversationWatcher from "./conversations/deleteConversation";

function* rootSaga() {
	yield all([
		fork(createUserWatcher),
		fork(userLoginWatcher),
		fork(fetchConversationsWatcher),
		fork(createConversationsWatcher),
		fork(viewConversationWatcher),
		fork(editConversationWatcher),
		fork(deleteConversationWatcher)
	]);
}

export default rootSaga;
