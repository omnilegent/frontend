import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

function* viewConversationWorker(action) {
	try {
		const { data } = yield call(axios, `/api/web/view/${action.id}`);
		yield put({
			type: "VIEW_CONVERSATION_SUCCESS",
			conversation: data
		});
	} catch (e) {
		yield put({
			type: "ERROR_ADD",
			errorMsg: "Couldn't load conversation ;x"
		});
	}
}

function* viewConversationWatcher() {
	yield takeLatest("VIEW_CONVERSATION", viewConversationWorker);
}

export default viewConversationWatcher;
