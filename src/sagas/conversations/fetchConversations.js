import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

function* fetchConversationsWorker() {
	try {
		yield call(console.log, "fetchConversationsWorker");
		const { data: conversations } = yield call(axios, "/api/web");
		console.log("conversations", conversations);
		yield put({
			type: "FETCH_CONVERSATIONS_SUCCESS",
			conversations
		});
	} catch (err) {
		yield put({
			type: "ERROR_ADD",
			errorMsg: "Couldn't load conversations"
		});
	}
}

function* fetchConversationsWatcher() {
	yield takeLatest("FETCH_CONVERSATIONS", fetchConversationsWorker);
}

export default fetchConversationsWatcher;
