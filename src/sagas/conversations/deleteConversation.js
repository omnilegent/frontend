import { call, put, takeLatest } from "redux-saga/effects";
import axios from "axios";

function* deleteConversationWorker(action) {
	try {
		const data = yield call(axios.delete, `/api/web/delete/${action.id}`);
		// TODO: connect to reducer
	} catch (err) {
		yield put({
			type: "ERROR_ADD",
			errorMsg: "Could not delete the conversation?!?"
		});
	}
}

function* deleteConversationWatcher() {
	yield takeLatest("DELETE_CONVERSATION", deleteConversationWorker);
}

export default deleteConversationWatcher;
