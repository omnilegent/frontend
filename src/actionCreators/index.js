import {
	USER_CREATE,
	USER_LOGIN,
	USER_LOGIN_SUCCESS,
	USER_LOGOUT
} from "../actions/index.js";

export const userCreateCreator = ({ email, password, confirmPassword }) => {
	console.log("userCreateCreator", email, password, confirmPassword);
	return {
		type: USER_CREATE,
		email,
		password
	};
};

export const userLoginCreator = ({ email, password }) => {
	console.log("userLoginCreator", email, password);
	return {
		type: USER_LOGIN,
		email,
		password
	};
};

export const userLoginSuccessCreator = ({ email, password }) => {
	console.log("userLoginSuccessCreator", email, password);
	return {
		type: USER_LOGIN_SUCCESS,
		email,
		password
	};
};

export const userLogoutCreator = () => {
	console.log("userLogoutCreator");
	return {
		type: USER_LOGOUT
	};
};
