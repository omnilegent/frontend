import {
	FETCH_CONVERSATIONS,
	CREATE_CONVERSATION,
	VIEW_CONVERSATION,
	EDIT_CONVERSATION,
	DELETE_CONVERSATION
} from "../actions/index";

export const fetchConversationsCreator = () => {
	console.log("fetchConversationsCreator");
	return {
		type: FETCH_CONVERSATIONS
	};
};

export const createConversationCreator = action => {
	console.log("action is", action);
	return {
		type: CREATE_CONVERSATION,
		...action
	};
};

export const viewConversationCreator = id => {
	return {
		type: VIEW_CONVERSATION,
		id
	};
};

export const editConversationCreator = action => {
	console.log("editConversationCreator action", action);
	return {
		type: EDIT_CONVERSATION,
		...action
	};
};

export const deleteConversationCreator = id => {
	return {
		type: DELETE_CONVERSATION,
		id
	};
};
