import React, { Component } from "react";
import { connect } from "react-redux";
import Editor from "react-medium-editor";

import ProtectedRoute from "../../../components/ProtectedRoute/ProtectedRoute";
import UserNavbar from "../../components/UserNavbar/UserNavbar";
import Button from "../../../components/Button/Button";
import Tag from "../../../components/Tag/Tag";
import Footer from "../../../components/Footer/Footer";
import {
	viewConversationCreator,
	editConversationCreator,
	deleteConversationCreator
} from "../../../actionCreators/conversations";

import styles from "./ViewConversation.module.scss";

class ViewConversation extends Component {
	state = {
		id: this.props.location.pathname.split("/").pop(),
		edit: false,
		title: "",
		description: "",
		text: "",
		tags: [""]
	};

	componentDidMount() {
		this.props.viewConversation(this.state.id);
	}

	componentDidUpdate() {
		try {
			if (!this.state.text) {
				this.fillState();
			}
		} catch (e) {}
	}

	fillState = () => {
		const { title, description, text, tags, id } = this.props.conversations[0];
		this.setState((prevState, prevProps) => ({
			id,
			title,
			description,
			text,
			tags
		}));
	};

	edit = () => {
		this.setState((prevState, prevProps) => ({
			edit: !prevState.edit
		}));
	};

	save = () => {
		this.props.editConversation(this.state);
		setTimeout(() => this.props.viewConversation(this.state.id), 100);
	};
	delete = () => {
		this.props.deleteConversation(this.state.id);
	};

	handleChange = (text, medium) => {
		this.setState((prevState, prevProps) => ({ text }));
	};

	render() {
		const {
			// id,
			title = "Conversation title",
			description = "Description",
			text = "Text"
			// tags,
			// createdAt
		} = this.props.conversations[0] || [];

		const conversationView = (
			<div className={styles.conversation}>
				<h1 className={styles.title}>{title}</h1>
				<p className={styles.description}>{description}</p>
				{/* Don't do this at home kids */}
				<div dangerouslySetInnerHTML={{ __html: text }} />
				<div className={styles["conversation-options"]}>
					<div className={styles["conversation-left"]}>
						<Tag text="food" />
						<Tag text="sex" />
					</div>
					<div className={styles["conversation-right"]}>
						<Button type="secondary" text="Edit" onClick={this.edit} />
					</div>
				</div>
			</div>
		);

		const conversationEdit = (
			<div className={styles.conversation}>
				<div className={styles["input-group"]}>
					<input
						className={styles["input-title"]}
						name="title"
						type="text"
						required
						placeholder="Conversation title"
						value={this.state.title}
						onChange={e => {
							e.persist(); // TODO: find alternative
							this.setState((prevState, prevProps) => ({
								title: e.target.value
							}));
						}}
					/>
					<input
						className={styles["input-description"]}
						type="text"
						required
						placeholder="Description"
						size="100"
						value={this.state.description}
						onChange={e => {
							e.persist();
							this.setState((prevState, prevProps) => ({
								description: e.target.value
							}));
						}}
					/>
				</div>
				<Editor text={this.state.text} onChange={this.handleChange} />
				<div className={styles["conversation-options"]}>
					<div className={styles["conversation-left"]}>
						<Button
							to="/home"
							type="delete"
							text="Delete"
							onClick={this.delete}
						/>
					</div>
					<div className={styles["conversation-right"]}>
						<Button type="outline" text="Cancel" onClick={this.edit} />
						<Button
							type="primary"
							text="Save"
							onClick={() => {
								this.save();
								this.edit();
							}}
						/>
					</div>
				</div>
			</div>
		);
		return (
			<ProtectedRoute>
				<div className={styles["view"]}>
					<div className={styles.navbar}>
						<UserNavbar type="light" />
					</div>

					<div className={styles.main}>
						{this.state.edit ? conversationEdit : conversationView}
					</div>

					<div className={styles.footer}>
						<Footer />
					</div>
				</div>
			</ProtectedRoute>
		);
	}
}

export default connect(
	state => state,
	dispatch => ({
		viewConversation: id => dispatch(viewConversationCreator(id)),
		editConversation: ({ id, title, description, text, tags }) =>
			dispatch(editConversationCreator({ id, title, description, text, tags })),
		deleteConversation: id => dispatch(deleteConversationCreator(id))
	})
)(ViewConversation);
