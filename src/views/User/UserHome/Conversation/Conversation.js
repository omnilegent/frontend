import React, { Component } from "react";

import styles from "./Conversation.module.scss";
import showMoreButton from "../../../../assets/images/show-conversation-back.svg";
import showLessButton from "../../../../assets/images/show-less-btn.svg";
import Button from "../../../../components/Button/Button";
import Tag from "../../../../components/Tag/Tag";

class Conversation extends Component {
	state = {
		back: false
	};
	showBack = () => {
		this.setState((prevState, prevProps) => ({
			back: true
		}));
	};
	hideBack = () => {
		this.setState((prevState, prevProps) => ({
			back: false
		}));
	};
	render() {
		const {
			id,
			title,
			description,
			// text,
			// tags = [1, 2],
			createdAt
		} = this.props;
		const date = new Date(createdAt).toDateString();
		return (
			<div
				className={
					this.props.size === "lg"
						? styles["conversation-lg"]
						: styles.conversation
				}
				onMouseLeave={this.hideBack}
			>
				<div className={styles["conversation-front"]}>
					<img
						className={styles["show-more-info"]}
						onClick={this.showBack}
						src={showMoreButton}
						alt="Show more conversation info"
					/>
				</div>
				<div
					className={`${styles["conversation-back"]} ${
						this.state.back
							? styles["conversation-back-show"]
							: styles["conversation-back-hide"]
					}`}
					onClick={this.hideBack}
				>
					<div className={styles["flex-row"]}>
						<span className={styles["conversation-date"]}>{date}</span>
						<img
							className={styles["conversation-show-less"]}
							src={showLessButton}
							alt="Show less iconversation info"
						/>
					</div>
					<h2 className={styles["conversation-title"]}>{title}</h2>
					{/* Limit summary to 50-100 characters to limit responsive issues with overflow of content */}
					<p className={styles["conversation-summary"]}>{description}</p>
					<div className={styles["flex-row"]}>
						<div className={styles["flex-row"]}>
							{/* {tags.map(tag => (
								<Tag text="test" />
							))} */}
							<Tag text="redhead" />
							<Tag text="sex" />
						</div>
						<Button type="primary" to={`/view/${id}`} text="Read more" />
					</div>
				</div>
			</div>
		);
	}
}

export default Conversation;
