import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Cookies from "universal-cookie";

import styles from "./UserNavbar.module.scss";
import dropdownIcon from "../../../assets/images/Dropdown-icon.svg";
import userSettingsIcon from "../../../assets/images/settings-icon.svg";
import userLogoutIcon from "../../../assets/images/logout-icon.svg";
import { userLogoutCreator } from "../../../actionCreators/index";

const cookies = new Cookies();

class UserNavbar extends Component {
	state = {
		userActionButtons: false
	};
	showUserActionButtons = () => {
		this.setState((prevState, prevProps) => ({
			userActionButtons: true
		}));
	};
	hideUserActionButtons = () => {
		this.setState((prevState, prevProps) => ({
			userActionButtons: false
		}));
	};
	logout = () => {
		// delete cookie
		cookies.remove("access-token");
		// redirect to /login
		this.props.logout();
	};
	render() {
		return this.props.type === "light" ? (
			<div className={styles["user-navbar-light"]}>
				<div className={styles.brand}>
					<Link to="/home">Jouska</Link>
				</div>
				<div className={styles["user-icon"]} />
			</div>
		) : (
			<div className={styles["user-navbar"]}>
				<img
					className={styles["dropdsown-icon"]}
					src={dropdownIcon}
					alt="dropdown icon"
				/>
				<div className={styles["welcome-text"]}>
					<h1 className={styles["welcome-heading"]}>Welcome Ryan Centaur</h1>
					<span className={styles["welcome-subheading"]}>
						You have 1337 conversations in your library
					</span>
				</div>
				<div
					onMouseLeave={this.hideUserActionButtons}
					className={styles["user-actions"]}
				>
					<div
						onMouseEnter={this.showUserActionButtons}
						className={styles["user-icon"]}
					/>
					<div
						className={`${styles["user-action-buttons"]} ${
							this.state.userActionButtons ? styles.show : ""
						}`}
					>
						<div className={styles["user-settings-button"]}>
							<img src={userSettingsIcon} alt="User settings icon" />
						</div>
						<div onClick={this.logout} className={styles["user-logout-button"]}>
							<img src={userLogoutIcon} alt="User logout icon" />
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(
	({ main }) => ({ ...main }),
	dispatch => ({
		logout: () => dispatch(userLogoutCreator())
	})
)(UserNavbar);
