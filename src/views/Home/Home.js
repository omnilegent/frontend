import React from "react";
import Navbar from "../../components/Navbar/Navbar";
import Footer from "../../components/Footer/Footer";

import styles from "./Home.module.scss";
import utils from "../../assets/styles/utils.scss";

// Images
import welcomeImage from "../../assets/images/undraw_welcome_secondary.svg";
import conversationImage from "../../assets/images/undraw_conversation_secondary.svg";
import starmanImage from "../../assets/images/undraw_starman_secondary.svg";

export default function Home() {
	console.log("utils", utils);
	return (
		<div className={styles.home}>
			<div className={styles.navbar}>
				<Navbar />
			</div>
			<div className={styles.main}>
				<div className={styles["home-story"]}>
					<div className={`${styles["story-part"]}`}>
						<div className="container">
							<h1>Welcome to Jouska</h1>
							<img src={welcomeImage} alt="welcome illustration" />
						</div>
					</div>
					<div className={`${styles["story-part"]}`}>
						<div className="container">
							<img src={conversationImage} alt="conversation illustration" />
							<h2>because some of the best conversations</h2>
						</div>
					</div>
					<div className={`${styles["story-part"]}`}>
						<div className="container">
							<h2>happen all in our heads...</h2>
							<img src={starmanImage} alt="astronaut illustration" />
						</div>
					</div>
				</div>
			</div>
			<div className={styles.footer}>
				<Footer />
			</div>
		</div>
	);
}
