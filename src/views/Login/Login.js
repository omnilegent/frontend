import React, { Component } from "react";
import Modal from "react-modal";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import styles from "./Login.module.scss";
import Navbar from "../../components/Navbar/Navbar";
import Button from "../../components/Button/Button";
import Footer from "../../components/Footer/Footer";
import loginImage from "../../assets/images/undraw_authentication_secondary.svg";
import ModalForm from "../../components/ModalForm/ModalForm";

const customStyles = {
	content: {
		top: "50%",
		left: "50%",
		right: "auto",
		bottom: "auto",
		marginRight: "-50%",
		transform: "translate(-50%, -50%)",
		width: "642px",
		height: "600px",
		padding: "50px 71px",
		boxSizing: "border-box",
		boxShadow:
			"0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14),		0 1px 5px 0 rgba(0, 0, 0, 0.12)"
	}
};

Modal.setAppElement("#root");

class Login extends Component {
	state = {
		modalIsOpen: false
	};

	openModal = () => {
		this.setState({ modalIsOpen: true });
	};

	afterOpenModal = () => {
		// references are now sync'd and can be accessed.
		// this.subtitle.style.color = "#f00";
	};

	closeModal = () => {
		this.setState({ modalIsOpen: false });
	};
	render() {
		if (this.props.authenticated) return <Redirect to="/home" />;

		return (
			<div className={styles.login}>
				<div className={styles.navbar}>
					<Navbar />
				</div>
				<div className={styles.main}>
					<div className={styles.container}>
						<img src={loginImage} alt="Log in illustration" />
						<div className={styles["login-options"]}>
							<Button
								to="/login/google"
								type="google"
								text="Log in with Google"
								size="lg"
							/>
							<Button
								to="/login/facebook"
								type="facebook"
								text="Log in with Facebook"
								size="lg"
							/>
							<Button
								to="/login/spotify"
								type="spotify"
								text="Log in with Spotify"
								size="lg"
							/>
							<Button
								type="outline"
								text="Log in with Email"
								size="lg"
								onClick={this.openModal}
							/>
						</div>
						<Modal
							isOpen={this.state.modalIsOpen}
							onAfterOpen={this.afterOpenModal}
							onRequestClose={this.closeModal}
							style={customStyles}
							contentLabel="Example Modal"
						>
							<ModalForm type="login" />
						</Modal>
					</div>
				</div>
				<div className={styles.footer}>
					<Footer />
				</div>
			</div>
		);
	}
}

export default connect(
	({ main }) => ({ ...main }),
	{}
)(Login);
