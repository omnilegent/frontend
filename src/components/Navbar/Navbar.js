import React from "react";
import { Link } from "react-router-dom";

import styles from "./Navbar.module.scss";
import Button from "../Button/Button";

export default function Navbar() {
	return (
		<div className={styles.navbar}>
			<div className={styles.brand}>
				<Link to="/">Jouska</Link>
			</div>
			<div className={styles["navbar-options"]}>
				<Button to="/signup" type="primary" text="Sign up" />
				<Button to="/login" type="outline" text="Log in" />
			</div>
		</div>
	);
}
