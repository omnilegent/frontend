import React, { Component } from "react";

import styles from "./Input.module.scss";

// describe arguments
class Input extends Component {
	state = {
		focus: false
	};
	focusLabel = () => {
		this.setState((prevState, prevProps) => ({
			focus: true
		}));
	};
	blurLabel = () => {
		this.setState((prevState, prevProps) => ({
			focus: false
		}));
	};
	render() {
		const { focus } = this.state;
		const { label, labelText, type, placeholder } = this.props;
		return (
			<div className={styles["form-group"]}>
				<label
					className={focus ? styles["label-focus"] : styles["label-hide"]}
					htmlFor={label}
				>
					{labelText}
				</label>
				<input
					type={type}
					name={label}
					placeholder={placeholder}
					onFocus={this.focusLabel}
					onBlur={this.blurLabel}
					// required
				/>
			</div>
		);
	}
}

export default Input;
