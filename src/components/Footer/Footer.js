import React from "react";

import styles from "./Footer.module.scss";
import footerLogo from "../../assets/images/undraw_programming_2svr-secondary.svg";

export default function Footer() {
	return (
		<footer className={styles.footer}>
			<div className={styles["footer-text"]}>
				Created for fun and no profit by Momchil Kolev
			</div>
			<img
				src={footerLogo}
				className={styles["footer-image"]}
				alt="programmer illustration"
			/>
		</footer>
	);
}
