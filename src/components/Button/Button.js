import React from "react";
import { Link } from "react-router-dom";

import styles from "./Button.module.scss";

/*
 * type {string} - type of button to show
 * to {string} - link to link to
 * text {string} - text to show inside the button
 */
export default function Button(props) {
	const { type, to, text, size, onClick } = props;
	let button;
	if (to)
		button = (
			<Link
				to={to}
				className={`${styles.btn} ${type ? `${styles[`btn-${type}`]}` : ""} ${
					size ? `${styles[`btn-${size}`]}` : ""
				}`}
				onClick={onClick}
			>
				{/* Customize to accept non-link */}
				{text}
			</Link>
		);
	else if (type === "submit")
		button = (
			<button
				className={`${styles.btn} ${type ? `${styles[`btn-${type}`]}` : ""} ${
					size ? `${styles[`btn-${size}`]}` : ""
				}`}
				onClick={onClick}
				type="submit"
			>
				{text}
			</button>
		);
	else
		button = (
			<div
				className={`${styles.btn} ${type ? `${styles[`btn-${type}`]}` : ""} ${
					size ? `${styles[`btn-${size}`]}` : ""
				}`}
				onClick={onClick}
			>
				{text}
			</div>
		);
	return button;
}
