import React from "react";
import { withRouter } from "react-router-dom";

import SignupForm from "./SignupForm/SignupForm";
import LoginForm from "./LoginForm/LoginForm";

const ModalForm = props => {
	const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	if (props.type === "signup") return <SignupForm emailRegex={emailRegex} />;
	else if (props.type === "login") return <LoginForm emailRegex={emailRegex} />;
};
export default withRouter(ModalForm);
