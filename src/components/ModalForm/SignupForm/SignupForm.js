import React from "react";
import { connect } from "react-redux";

import Button from "../../Button/Button";
import Input from "../../Input/Input";
import styles from "../ModalForm.module.scss";
import {
	errorAddCreator,
	errorRemoveCreator,
	errorResetCreator
} from "../../../actionCreators/errors";
import { userCreateCreator } from "../../../actionCreators/index";

const SignupForm = props => {
	return (
		<div className={styles["form-container"]}>
			<form
				className={styles.form}
				onSubmit={e => {
					e.preventDefault();
					let {
						elements,
						elements: { email, password, confirmPassword = {} }
					} = e.target;
					// debugger;
					if (email.value === "" || props.emailRegex.test(email.value)) {
						if (!props.errors.includes("Please enter a valid email address"))
							props.addError("Please enter a valid email address");
					} else if (!confirmPassword.value) {
						if (!props.errors.includes("Please confirm your password")) {
							props.resetErrors();
							props.addError("Please confirm your password");
						}
					} else if (
						password.value !== confirmPassword.value ||
						(password.value === "" || confirmPassword.value === "")
					) {
						if (!props.errors.includes("Passwords don't match")) {
							props.resetErrors();
							props.addError("Passwords don't match");
						}
					} else {
						props.resetErrors();
						let action = {};
						for (let el of elements) {
							action[el.name] = el.value;
						}
						props.createUser(action);
					}
				}}
			>
				{props.errors.map((err, i) => (
					<span key={i} className={styles["error-message"]}>
						{err}
					</span>
				))}
				<h3>Sign up with email</h3>
				<Input
					label="email"
					labelText="Email"
					type="email"
					placeholder="Enter your email address"
				/>
				<Input
					label="password"
					labelText="Password"
					type="password"
					placeholder="Enter your password"
				/>
				<Input
					label="confirmPassword"
					labelText="Confirm Password"
					type="password"
					placeholder="Confirm your email address"
				/>
				<Button type="submit" text="Create Account" />
			</form>
		</div>
	);
};

export default connect(
	({ main, errors }) => ({ ...main, errors }),
	dispatch => ({
		addError: errorMsg => dispatch(errorAddCreator(errorMsg)),
		removeError: errorMsg => dispatch(errorRemoveCreator(errorMsg)),
		resetErrors: () => dispatch(errorResetCreator()),
		createUser: action => dispatch(userCreateCreator(action))
	})
)(SignupForm);
